from pathlib import Path
from datetime import date
import json
import requests


def get_tokens(cookies_path):
    if cookies_path.is_file():
        input_str = cookies_path.read_text(encoding="utf-8").strip()
    else:
        raise FileNotFoundError("The file path does not exist:", cookies_path)
    return json.loads(input_str)


def write_data_file(day, year, name, cookie):
    # Create file directory
    local_dir = f"data/{year}/{day:02}/"
    in_file_dir = Path.cwd() / f"{local_dir}"
    Path(in_file_dir).mkdir(parents=True, exist_ok=True)

    # Check if the file already exists.
    # AOC will rate limit you if you spam it with too may requests,
    # so best not to pull data we aready have
    file_name = f'{local_dir}day{day:02}_{name}.txt'
    file_path = Path.cwd() / f"./{file_name}"
    if file_path.is_file():
        print(f"WARNING: File already exists, skipping: {file_path}")
    else:
        # Get the data
        headers = {'session': cookie}
        url = f'https://adventofcode.com/{year}/day/{day}/input'

        session = requests.Session()
        resp = session.get(url, cookies=headers)

        with file_path.open(mode="w", encoding="utf-8") as in_file:
            in_file.write(resp.text)


if __name__ == "__main__":
    INPUT_PATH_TOKENS = Path.cwd() / "./config/tokens.json"
    tokens = get_tokens(INPUT_PATH_TOKENS)

    for year in range(2015, int(date.today().year) + 1):
        for day in range(1, 26):
            for cookie_key, cookie_val in tokens.items():
                write_data_file(
                    day=day,
                    year=year,
                    name=cookie_key,
                    cookie=cookie_val)
