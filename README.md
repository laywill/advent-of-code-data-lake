# Advent of Code Data Lake

<!-- ## Distribution

The contents of this repo are not to be distributed outside of those invited to the repo. -->

## Contributing

### Adding your AOC Session Cookies

- Log in to [https://adventofcode.com/](https://adventofcode.com/)
- Go to any puzzle input page, e.g. [https://adventofcode.com/2021/day/1/input](https://adventofcode.com/2021/day/1/input)
- Follow [these steps](https://github.com/wimglenn/advent-of-code-wim/issues/1) to find your Cookie value.
- Copy and paste your session ID cookie value to `config/tokens.json`, giving it a key that is meaningful and unique
- You could add multiple session tokens, for example one logged in with each of: GitHub, Google, Reddit.

### Code Improvements

- Please open an issue, describing your improvement.
- Use the `Create MR` button to generate a Merge Request and a branch from your issue.
- MRs must be reviewed before merging.

## Using the tool

### Create a virtual environment

On MacOS and Linux:

```bash
python3 -m venv venv
```

On Windows:

```powershell
python -m venv venv
```

### Activate virtual environment

On MacOS and Linux:

```bash
source venv/bin/activate
```

On Windows:

```powershell
.\venv\Scripts\activate
```

### Install the Data Harvesting Tool Dependencies

```powershell
pip install -r requirements.txt
```

### Run the data Harvester

```powershell
python ./aoc_get_data_lake.py
```

## FAQ

### [Is it kosher to share puzzle inputs and answers after the fact?](https://www.reddit.com/r/adventofcode/comments/7lesj5/comment/drlt9am/)

Eric Wastl:

> I don't mind having a few of the inputs posted, please don't go on a quest to collect many or all of the inputs for every puzzle. Doing so makes it that much easier for someone to clone and steal the whole site. I put tons of time and money into Advent of Code, and the many inputs are one way I prevent people from copying the content.

By not posting the data itself in this repo, only providing the means to get it via session cookies, we can locally harvest the data without risking exposing it publically on the internet.

### [Are everyone's input data (and by extension, solutions) different?](https://www.reddit.com/r/adventofcode/comments/e7khy8/comment/fa13hb9/)

Danielle Lucek:

> Please don't share your input anywhere as that makes it easier for unscrupulous folks to reverse-engineer all the hard work that /u/topaz2078 has put into this event.
>
> However, definitely do consider sharing your solutions (the actual code that gets you the answer that you input on adventofcode.com) on the daily megathreads! There's a calendar on the sidebar with a link to each day's megathread.

OK, we aren't sharing any inputs here. Just providing a means to acquire them.
